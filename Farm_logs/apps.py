from django.apps import AppConfig


class FarmLogsConfig(AppConfig):
    name = 'Farm_logs'
