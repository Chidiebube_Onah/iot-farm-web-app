from django.shortcuts import render
import requests
from django.views.decorators.csrf import csrf_exempt

from django.http import HttpResponse
# Create your views here.
@csrf_exempt
def index(request):
    response = requests.get('https://api.thingspeak.com/channels/738506/feeds/last.json?api_key=NGPCK4UA2YOEL935')
    geodata = response.json()
    return render(request, 'data.html', {
        'field1': float(geodata['field1'])
        # 'field2': geodata['field2']
    })
